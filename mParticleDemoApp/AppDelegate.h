//
//  AppDelegate.h
//  mParticleDemoApp
//
//  Created by Brock Hardman on 4/12/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

