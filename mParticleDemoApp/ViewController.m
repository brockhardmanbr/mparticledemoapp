//
//  ViewController.m
//  mParticleDemoApp
//
//  Created by Brock Hardman on 4/12/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "ViewController.h"
#import <mParticle/mParticle.h>

@interface ViewController ()

@property (nonatomic, copy) NSString *accessToken;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[MParticle sharedInstance] beginSession];
    
    [[MParticle sharedInstance] logEvent:@"Main Screen Loading" eventType:MPEventTypeTransaction eventInfo:@{ @"timeLogged" : [NSDate date] }];
}

- (IBAction)tooMuchTorchysTapped:(UIButton *)sender
{
    [[MParticle sharedInstance] logEvent:@"Too Much Torchys" eventType:MPEventTypeTransaction eventInfo:@{ @"result" : @"Don't go in there for 35-45 minutes" }];
}

- (IBAction)tooMuchCoffeeTapped:(UIButton *)sender
{
    [[MParticle sharedInstance] logEvent:@"Too Much Coffee" eventType:MPEventTypeTransaction eventInfo:@{ @"result" : @"Prepare for crash landing" }];
}

- (IBAction)momGoesCollegeTapped:(UIButton *)sender
{
    [[MParticle sharedInstance] logEvent:@"Mom Goes To College" eventType:MPEventTypeTransaction eventInfo:@{ @"result" : @"Kip just laughed at you" }];
}

- (IBAction)whoLetDogsOutTapped:(UIButton *)sender
{
    [[MParticle sharedInstance] logEvent:@"Who Let Dogs Out" eventType:MPEventTypeTransaction eventInfo:@{ @"result" : @"Your mom, after she graduates college" }];
}

- (IBAction)whatDidFoxSayTapped:(UIButton *)sender
{
    [[MParticle sharedInstance] logEvent:@"What Did Fox Say" eventType:MPEventTypeTransaction eventInfo:@{ @"result" : @"Ring ling a ding a ding a ding ding" }];
}

- (IBAction)doNotPressTapped:(UIButton *)sender
{
    [[MParticle sharedInstance] logEvent:@"Do Not Press" eventType:MPEventTypeTransaction eventInfo:@{ @"result" : @"You just launched the nuke sequence" }];
}

@end
