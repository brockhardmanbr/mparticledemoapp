//
//  AppDelegate.m
//  mParticleDemoApp
//
//  Created by Brock Hardman on 4/12/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "AppDelegate.h"
#import <mParticle/mParticle.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    MParticle *mParticle = [MParticle sharedInstance];
    
    [mParticle startWithKey:@"40dd5983602f9643bdfd99dcf86726ef"
                     secret:@"-usfdZLigkSGD28745TNunUO_mMjg_xrZ6LQAMA_ghoyob6k9k75SL1_XJA0YcXy"];
    
    return YES;
}

@end
